<!DOCTYPE HTML>  
<html>
<head>
</head>
<body>  

<?php

$name = $email = $gender = $comment = $website = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $name = test_name($_POST["name"]);
  $email = ($_POST["email"]);
  $website = ($_POST["website"]);
  $comment = test_comment($_POST["comment"]);
  $gender = ($_POST["gender"]);
}

function test_name($data) {
  $data = trim($data);
  $data = ucfirst($data);
  return $data;
}

function test_comment($key){
    $key = trim($key);
    $key = ucwords($key);
    return $key;
}

?>

<h2>PHP Form Validation Example</h2>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  Name: <input type="text" name="name">
  <br><br>
  E-mail: <input type="text" name="email">
  <br><br>
  Website: <input type="text" name="website">
  <br><br>
  Comment: <textarea name="comment" rows="7" cols="40"></textarea>
  <br><br>
  Gender:
  <input type="radio" name="gender" value="female">Female
  <input type="radio" name="gender" value="male">Male
  <input type="radio" name="gender" value="other">Other
  <br><br>
  <input type="submit" name="submit" value="Submit">  
</form>

<?php
echo "<h2>Inputs are:</h2>";
echo $name;
echo "<br>";
echo $email;
echo "<br>";
echo $website;
echo "<br>";
echo $comment;
echo "<br>";
echo $gender;
echo "<br/>";
?>

</body>
</html>