<!DOCTYPE html>
<html>
<head>
	<title>Registration form</title>
</head>
<body>

<?php

$Firstname = $Lastname = $Address = $Gender = $Phone = $Email = $User = $Pass= "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $Firstname = test_name($_POST["firstname"]);
  $Lastnamename =test_name ($_POST["lastname"]);
  $Address = test_name($_POST["address"]);
  $Gender = test_name($_POST["gender"]);
  $Phone = test_name($_POST["phone"]);
  $Email = test_name($_POST["email"]);
  $User = test_name($_POST["username"]);
  $Pass = test_password($_POST["password"]);
}

function test_name($data) {
  $data = trim($data);
  $data = ucfirst($data);
  return $data;
}

function test_password($key){
    $key = trim($key);
    $key = ucwords($key);
    return $key;
}

?>

<h2>Form Validation</h2>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method= "POST">
	<table>
		<tr>
			<td>Name:</td>	
			<td><input type="text" name="firstname" placeholder="Enter your firstname"></td>
			<td><input type="text" name="lastname" placeholder="Enter your lastname"></td>
		</tr>

		<tr>
			<td>Address:</td>	
			<td><input type="text" name="address" placeholder="Enter your address"></td>
		</tr>

		<tr>
			<td>Gender:</td>
			<td><input type="radio" name="gender" value="Male">Male</td>
			<td><input type="radio" name="gender" value="Female">Female</td>	
		</tr>

		<tr>
			<td>Phone:</td>
			<td><input type="tel" name="phone" placeholder="Enter your phone number"></td>
		</tr>

		<tr>
			<td>Email:</td>
			<td><input type="email" name="email"placeholder="example@example.com"></td>
		</tr>

		<tr>
			<td>Username:</td>
			<td><input type="text" name="username" placeholder="Enter username"></td>
		</tr>

		<tr>
			<td>Password:</td>
			<td><input type="Password" name="Password" placeholder="Enter your Password"></td>
		</tr>

		<tr>
			<td><input type="submit" name="submit" value="SignUp"></td>
		</tr>

	</table>
    </form>
	

<?php
echo "<h2>Inputs are:</h2>";
echo $Firstname;
echo "<br>";
echo $Lastname;
echo "<br>";
echo $Address;
echo "<br>";
echo $Gender;
echo "<br>";
echo "<br>";
echo $Phone;
echo "<br>";
echo $Email;
echo "<br>";
echo $User;
echo "<br>";
echo $Pass;
echo "<br>";
?>

</body>
</html>